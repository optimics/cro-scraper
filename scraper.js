/*

         .'.         .'.
        |  \       /  |
        '.  \  |  /  .'
          '. \\|// .'
            '-- --'
            .'/|\'.^~DLF
           '..'|'..'

  Fairy' miracle data scraper...


*/

// define channels

var channelList = {
  "5a0d64dde3a3ab07c525e902":"CRo D-DUR",
  "5a0d6539e3a3ab3aa24d6322":"CRo DVOJKA",
  "5a0d6584e3a3ab07c525e903":"CRo JAZZ",
  "5a0d65bce3a3ab07c525e904":"CRo PLUS",
  "5a0d65f2e3a3ab07c525e905":"CRo RADIO JUNIOR",
  "5a0d6627e3a3ab1cf9267922":"CRo RADIO WAVE",
  "5a0d6674e3a3ab3aa24d6323":"CRo RADIOZURNAL",
  "5a0d66a5e3a3ab3aa24d6324":"CRo VLTAVA",
  "5e8d9e6de3a3ab51245ca3aa":"CRo Radio DAB Praha",
  "5e8d9ebee3a3ab487a549b57":"CRo REGION",
  "5e8d9eece3a3ab487a549b58":"CRo RADIO PRAGUE Int",
  "5e8d9f27e3a3ab51245ca3ad":"CRo BRNO",
  "5e8d9f4de3a3ab1b41060a05":"CRo C.BUDEJOVICE",
  "5e8d9f72e3a3ab1b41060a06":"CRo H.KRALOVE",
  "5e8d9f9fe3a3ab51245ca3b0":"CRo KARLOVY VARY",
  "5e8d9fd1e3a3ab51245ca3b1":"CRo LIBEREC",
  "5e8da008e3a3ab1b41060a07":"CRo VYSOCINA",
  "5e8da033e3a3ab1b41060a08":"CRo PARDUBICE",
  "5e8da05ae3a3ab487a549b5c":"CRo PLZEN",
  "5e8da082e3a3ab51245ca3b4":"CRo OLOMOUC",
  "5e8da0b0e3a3ab487a549b5e":"CRo OSTRAVA",
  "5e8da0d7e3a3ab487a549b5f":"CRo SEVER",
  "5e8da101e3a3ab51245ca3b5":"CRo ZLIN",
};


// count longest element
var longest = "";
  for (var channelDetail in channelList) {
    if (channelList.hasOwnProperty(channelDetail)) {
      if(channelList[channelDetail].length > longest.length){
        longest = channelList[channelDetail];
      }
    }
  }

var lineLength = longest.length;

function detectEnvironment(){

  if(document.location.href.indexOf("https://hbbtv.cra.cz/") != -1){
    return "cra";
  }
  return undefined;
}

// Load CSS

var style = document.createElement('style');
 style.innerHTML = `
 /* The Modal (background) */
 .modal {
   display: none; /* Hidden by default */
   position: fixed; /* Stay in place */
   z-index: 1000; /* Sit on top */
   left: 0;
   top: 0;
   width: 100%; /* Full width */
   height: 100%; /* Full height */
   overflow: auto; /* Enable scroll if needed */
   background-color: rgb(0,0,0); /* Fallback color */
   background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
   clip: rect(auto, auto, auto, auto);
   color: white;
  font: 1.3rem Inconsolata, monospace;
  text-shadow: 0 0 5px #C8C8C8;
 }

 .modal form input, .modal form button {
   color: white;
  font: 1.3rem Inconsolata, monospace;
  text-shadow: 0 0 5px #C8C8C8;
  background: inherit;
  border : none;

  margin: 5px;
 }

 .modal form button {

 }

 .modal form input {
     width: 60%;
 }

 .modal form button:hover,
 .modal form button:focus {
   color: yellow;
   text-decoration: underline;
   cursor: pointer;
 }

 .modal ul li {
   white-space: pre-wrap;
   list-style-type: none;
 }

 /* Modal Content/Box */
 .modal-content2 {
   background-color: #fefefe;
   margin: 0px auto; /* 15% from the top and centered */
   padding: 20px;
   border: 1px solid #888;
   width: 80%; /* Could be more or less, depending on screen size */


 }
 .modal-content {
    width: 80%; /* Could be more or less, depending on screen size */
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);

    padding: 20px;

    background-color: black;
    background-image: radial-gradient(
      rgba(0, 150, 0, 0.75), black 120%
    );

}
 /* The Close Button */
 .close {
   color: yellow;
   float: right;
   font-size: 28px;
   font-weight: bold;
   position: absolute;
   top: 5px;
   right: 5px;
 }

 .close:hover,
 .close:focus {
   color: white;
   text-decoration: none;
   cursor: pointer;
 }

 #myBtn {
   cursor: pointer;
 }

 `;
 document.head.appendChild(style);

// Prepare HTML elements

// open button
var docHeader = document.querySelector(".header__logo");

var btn = document.createElement("button");
btn.id = "myBtn";
var buttonText  = document.createTextNode("Open Data Export Options");

btn.appendChild(buttonText);
docHeader.appendChild(btn);


// other
var docBody = document.getElementsByTagName("body")[0];

//main wrapper
var modal = document.createElement("div");
modal.id = "myModal";
modal.classList.add("modal");

// modal content
var modalContent = document.createElement("div");
modalContent.classList.add("modal-content");

// close button
var close = document.createElement("span");
close.classList.add("close");

var closeText  = document.createTextNode("exit");
close.appendChild(closeText);

modalContent.appendChild(close);

// content window
var modalHeadline = document.createElement("h1");
var modalText  = document.createTextNode("cro scraper v 0.1 - " + detectEnvironment() );
modalHeadline.appendChild(modalText);
modalContent.appendChild(modalHeadline);

// CRA section
if(detectEnvironment()==="cra"){

  // parame selection
  var setParams = document.createElement("div");
  var modalText  = document.createTextNode("$ " + detectEnvironment() + " ");

  var makeForm = document.createElement("form");
  makeForm.id = "configForm";
  var makeInput = document.createElement("input");
  makeInput.setAttribute("type", "text");
  makeInput.setAttribute("value", "--hour --lastMonth --allChannels");
  makeInput.setAttribute("name", "configDetail");
  makeForm.appendChild(makeInput);


  var makeSubmit = document.createElement("button");
  makeSubmit.setAttribute("type", "button");
  makeSubmit.setAttribute("id", "sendAll");
  var submitText  = document.createTextNode("<< process all >>");
  makeSubmit.appendChild(submitText);
  makeForm.appendChild(makeSubmit);

  setParams.appendChild(modalText);
  setParams.appendChild(makeForm);

  modalContent.appendChild(setParams);

  // available channels

  var list = document.createElement("ul");
  list.classList.add("channels");

  var chanlen = channelList.length;

  for (var channelDetail in channelList) {
    if (channelList.hasOwnProperty(channelDetail)) {
      var chnl = channelList[channelDetail];
      var linePrint = chnl;

      // allign lines to single column
      for(var i=0; i < (lineLength - chnl.length); i++ ){
        linePrint += " ";
      }
      //linePrint += " | download |";

      var progressWrapper = document.createElement("span");
      progressWrapper.id = channelDetail;
      var channelItem = document.createElement("li");
      var channelText = document.createTextNode(linePrint);

      channelItem.appendChild(channelText);
      channelItem.appendChild(progressWrapper);
      list.appendChild(channelItem);
      modalContent.appendChild(list);
    }
  }
}
// end of CRA section
else {

}


// write modal content to DOM
modal.appendChild(modalContent);
//docBody.appendChild(modal);

docBody.insertBefore(modal, docBody.firstChild);

/*
<!-- Trigger/Open The Modal -->
<button id="myBtn">Open Modal</button>

<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
    <p>Some text in the Modal..</p>
  </div>

</div>

*/
// newly report in DM
function updateStatus(status,itemid){
  var item = document.getElementById(itemid);
  item.innerText += status;
}


function mergeObj(obj1,obj2){
    var obj3 = {};
    for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
    for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
    return obj3;
}

function DownloadManager(channelList){

  var channelList = channelList;
  var globalJobQueue = [];

  var indexToProcess = 0;
  var indexToDiscover = 0;

  var exportDiscoveries = [];
  var exportDataset = [];

  function channelDiscovery(channelList){

    var apiEndpoint = "https://hbbtv.cra.cz/en/analytic/portal/detail/";

    var apiFrom = 1583017200;
    var apiTo   = 1585691999;

    var discoveryQueue = [];

    for (var channelDetail in channelList) {
      if (channelList.hasOwnProperty(channelDetail)) {
        var chnl = channelList[channelDetail];

        var apiId = channelDetail;

        // discover applications
        var discoveryDetail = {
          id : apiId,
          from : apiFrom,
          to : apiTo,
          do : "applications"
        };

        var urlApps = apiEndpoint + discoveryDetail.id + "?from=" + discoveryDetail.from + "&to=" + discoveryDetail.to + "&id=" + discoveryDetail.id + "&do=" + discoveryDetail.do;

        discoveryQueue.push(mergeObj(discoveryDetail,{
          url : urlApps
        }));

        // discover distributions
        var discoveryDetail = {
          id : apiId,
          from : apiFrom,
          to : apiTo,
          do : "distribution"
        };

        var urlDist = apiEndpoint + discoveryDetail.id + "?from=" + discoveryDetail.from + "&to=" + discoveryDetail.to + "&id=" + discoveryDetail.id + "&do=" + discoveryDetail.do;

        discoveryQueue.push(mergeObj(discoveryDetail,{
          url : urlDist
        }));
      }
    }

    return discoveryQueue;
  }

  function discover(){
    var discoveryQueue = channelDiscovery(channelList);

    nextDiscovery(discoveryQueue);
  }

  function afterDiscoveryCallback(discoveryObject,discoveryQueue){
      return function(dataset){

        discoveryObject.dataset = JSON.parse(dataset);
        reportDiscovery(discoveryObject);
        processDownloadedDiscovery(discoveryObject);
        setTimeout(function(){
          nextDiscovery(discoveryQueue);
        },300);

      }
  }

  function reportDiscovery(discoveryObject){
    var discoveryObject = discoveryObject;
    var dataset = discoveryObject.dataset || [];

    for(var i = 0; i < dataset.length; i++){
      var item = document.getElementById(discoveryObject.id);

      if(discoveryObject.do === "applications"){
        item.innerText += " | " + dataset[i].name || "x";
      }

      if(discoveryObject.do === "distribution"){
        item.innerText += " | " + dataset[i].broadcastType || "x";
      }


    }


  }

  function processDownloadedDiscovery(discoveryObject) {
    console.log("Discovered dataset");
    console.log(discoveryObject.dataset);

    exportDiscoveries.push(discoveryObject);
  }

  // process each job
  function downloadDiscoveryData(discoveryObject,discoveryQueue){
    loadJSON(afterDiscoveryCallback(discoveryObject,discoveryQueue), discoveryObject.url);
    console.log(discoveryObject);
  }

  // recusion to process whole queue
  function nextDiscovery(discoveryQueue){

    if(indexToDiscover < discoveryQueue.length && indexToDiscover < 1500){
      downloadDiscoveryData(discoveryQueue[indexToDiscover],discoveryQueue);
      indexToDiscover = indexToDiscover+1;
    }
    else {
      finalizeDiscovery();
    }
  }

  function finalizeDiscovery(){
    console.log("I am done with discovery");
    console.dir(exportDiscoveries);
    console.log("I am generating");

    var generatedJobs = generateJobQueue(exportDiscoveries,channelList);
    console.dir(generatedJobs);
    globalJobQueue = generatedJobs;
    // init
    nextJob();
  }


  function generateJobQueue(exportDiscoveries,channelList){

    var exportDiscoveries = exportDiscoveries;
    var exportDiscoveriesLength = exportDiscoveries.length;
    var channelList = channelList;
    var channelListLength = channelList.length;
    var jobQueue = [];

    var apiEndpoint = "https://hbbtv.cra.cz/en/analytic/";
    var apiUniques  = "viewer/detail/";
    var apiChannels = "portal/detail/";
    var apiAppDetail = "app/detail/";

    var apiFrom = 1583017200;
    var apiTo   = 1585691999;

    var apiPeriod = "hour";

    // consolidate channel object with discoveries new approach
    var superChannelList = {};

    for (var channelDetail in channelList) {
      if (channelList.hasOwnProperty(channelDetail)) {
        var chnl = channelList[channelDetail];
        // copy old structure to new one
        var newChannelDetail = {
          channelId : channelDetail,
          channelName : channelList[channelDetail]
        };

        superChannelList[channelDetail] = newChannelDetail;

      }
    }

    // explore discoveries
    for(var j = 0; j < exportDiscoveriesLength; j++){
      var discoveryObject = exportDiscoveries[j];

      var newChannelsApps = [];
      var newChannelsDists= [];

      var dataset = discoveryObject.dataset || [];
      for(var i = 0; i < dataset.length; i++){

        if(discoveryObject.do === "applications"){
          newChannelsApps.push(dataset[i].appId);
        }
        if(discoveryObject.do === "distribution"){
          newChannelsDists.push({
            id : dataset[i].id,
            broadcastType : dataset[i].broadcastType
          });
        }
      }

      superChannelList[discoveryObject.id].channelApps = newChannelsApps;
      superChannelList[discoveryObject.id].channelDists = newChannelsDists;

    }

    console.dir(superChannelList);

    // generate from stationList

    for (var channelDetail in channelList) {
      if (channelList.hasOwnProperty(channelDetail)) {
        var chnl = channelList[channelDetail];

        var apiId = channelDetail;
        // generate generic channel reports
        var jobDetail = {
          id : channelDetail,
          from : apiFrom,
          to : apiTo,
          do : "period",
          period : apiPeriod,
          masterid : channelDetail
        };

        var urlAll = apiEndpoint + apiChannels + jobDetail.id + "?from=" + jobDetail.from + "&to=" + jobDetail.to + "&id=" + jobDetail.id + "&period=" + jobDetail.period + "&do=" + jobDetail.do;
        //console.log(urlAll);
        jobQueue.push(mergeObj(jobDetail,{
          url : urlAll,
          label : "all"
        }));

        // generate uniques
        var jobDetail = {
          id : channelDetail,
          from : apiFrom,
          to : apiTo,
          do : "period",
          period : apiPeriod,
          masterid : channelDetail
        };
        var urlUniq = apiEndpoint + apiUniques + jobDetail.id + "?from=" + jobDetail.from + "&to=" + jobDetail.to + "&id=" + jobDetail.id + "&period=" + jobDetail.period + "&do=" + jobDetail.do;
        //console.log(urlUniq);

        jobQueue.push(mergeObj(jobDetail,{
          url : urlUniq,
          type : "uniques",
          label: "uniques"
        }));

      }
    }

    console.log("I generated from channelList");

    // generate from discoveries
    for(var j = 0; j < exportDiscoveriesLength; j++){
      var discoveryObject = exportDiscoveries[j];


      // generate breakdowns
      var dataset = discoveryObject.dataset || [];
      for(var i = 0; i < dataset.length; i++){
        var item = document.getElementById(discoveryObject.id);

        // audio a html stream
        if(discoveryObject.do === "applications"){
          var jobDetail = {
            chId : discoveryObject.id,
            from : apiFrom,
            id : dataset[i].appId,
            to : apiTo,
            do : "period",
            period : apiPeriod,
            masterid : discoveryObject.id
          };

          var urlApp = apiEndpoint + apiAppDetail + jobDetail.id + "?chId=" + jobDetail.chId + "&from=" + jobDetail.from + "&to=" + jobDetail.to + "&id=" + jobDetail.id + "&period=" + jobDetail.period + "&do=" + jobDetail.do;
          //console.log(urlAll);
          jobQueue.push(mergeObj(jobDetail,{
            url : urlApp,
            label : dataset[i].name
          }));

          // generate breakdowns from superChannelList
          var availableDists = superChannelList[discoveryObject.id].channelDists;
          for(var h=0;h<availableDists.length;h++){
            jobDetail.dId = availableDists[h].id;

            var urlAppBreak = apiEndpoint + apiAppDetail + jobDetail.id + "?chId=" + jobDetail.chId + "&dId=" + jobDetail.dId + "&from=" + jobDetail.from + "&to=" + jobDetail.to + "&id=" + jobDetail.id + "&period=" + jobDetail.period + "&do=" + jobDetail.do;
            //console.log(urlAll);
            jobQueue.push(mergeObj(jobDetail,{
              url : urlAppBreak,
              label : dataset[i].name + " break by " + availableDists[h].broadcastType
            }));

          }


        }

        // sat and terr
        if(discoveryObject.do === "distribution"){

          var jobDetail = {
            dId : dataset[i].id,
            from : apiFrom,
            id : discoveryObject.id,
            to : apiTo,
            do : "period",
            period : apiPeriod,
            masterid : discoveryObject.id
          };

          var urlDis = apiEndpoint + apiChannels + jobDetail.id + "?dId=" + jobDetail.dId + "&from=" + jobDetail.from + "&to=" + jobDetail.to + "&id=" + jobDetail.id + "&period=" + jobDetail.period + "&do=" + jobDetail.do;
          //console.log(urlAll);
          jobQueue.push(mergeObj(jobDetail,{
            url : urlDis,
            label : dataset[i].broadcastType
          }));

        }

      }

    }

    return jobQueue;

  }



  // recusion to process whole queue
  function nextJob(){
    if(indexToProcess < globalJobQueue.length && indexToProcess < 5000){
      downloadData(globalJobQueue[indexToProcess]);
      indexToProcess = indexToProcess+1;
    }
    else {
      finalize();
    }
  }



  // aggregate data for export
  function processDownloaded(jobObject){
    console.log("Data Aggregated");
    console.log(jobObject.dataset);

    exportDataset.push(jobObject);
  }



  // load async data and fire callback
  function loadJSON(callback, url) {

     var xobj = new XMLHttpRequest();
         xobj.overrideMimeType("application/json");

     xobj.open('GET', url, true);

     xobj.setRequestHeader("X-Requested-With", "XMLHttpRequest");
     xobj.onreadystatechange = function () {
           if (xobj.readyState == 4 && xobj.status == "200") {
             // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
             callback(xobj.responseText);
           }
     };
     xobj.send(null);
  }

  // process each job
  function downloadData(jobObject){
    loadJSON(afterProcessCallback(jobObject), jobObject.url);
    console.log(jobObject);
  }

  // process each dataset
  function afterProcessCallback (jobObject){
    return function(dataset){
      report(jobObject.label,jobObject.masterid);
      jobObject.dataset = JSON.parse(dataset);
      processDownloaded(jobObject);
      setTimeout(function(){
        nextJob();
      },300);

    }
  }

  // update info table
  function report(status,itemid){
    var item = document.getElementById(itemid);
    item.innerText += " | " + status + "-ok";
  }

  // after all data fetched, do this
  function finalize(){
    console.log("This is the end");
    //download("cra_export_test.txt",dataset);
    console.dir(exportDataset);


    var makeDownloadAll = document.createElement("button");
    makeDownloadAll.setAttribute("type", "button");
    makeDownloadAll.setAttribute("id", "downloadAll");
    var makeDownloadAllText  = document.createTextNode("<< download all >>");
    makeDownloadAll.appendChild(makeDownloadAllText);
    makeForm.appendChild(makeDownloadAll);

    makeDownloadAll.addEventListener('click', function() {

      downloadManager.downloadDataset();

      makeDownloadAll.remove();

    }, false);

  }

  // save all exports to users computer
  function downloadFile(filename, text) {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);
    element.style.display = 'none';
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
  }

  // prepare data to download and inti download
  function makeDownload(){
    downloadFile("cra_export_test_full.json",JSON.stringify(exportDataset));
  }

  // public API
  this.init = nextJob;
  this.discover = discover;
  this.downloadDataset = makeDownload;
}

// public scope for current download batch
var downloadManager;

function doAllDownloads(){

  downloadManager = new DownloadManager(channelList);
  downloadManager.discover();
  /*
  var jobQueue = prepareJobQueue();

  downloadManager = new DownloadManager(jobQueue,channelList);
  downloadManager.init();

  console.dir(jobQueue);

  */
}


// Create UI


// When the user clicks on the button, open the modal

btn.addEventListener('click', function() {
  modal.style.display = "block";
  console.log("opening scrapers window");
}, false);

// When the user clicks on <span> (x), close the modal
close.addEventListener('click', function() {
  modal.style.display = "none";
}, false);

makeSubmit.addEventListener('click', function() {

  doAllDownloads();

}, false);



// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
