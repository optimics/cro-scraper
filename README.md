# CRo Scraper #

This scraper has to be injected into desired page and manually inititated via button *Open Data Export Options *

## Configuration ##

### date range ###
--lastMonth
--lastWeek
--lastDay

--from 153909878
--to 154889898

### granularity ###
--hour
--day
--month

### channels ###
--allChannels
(no more options available now)

## << Process all >> ##

After hitting *process all* button, discovery phase takes action.
During discovery phase, all channels API are asked for details about Distributions and Applications.
Based on their response, dataset breakdowns are generated in the following pattern:

 -all default, non breakdowned reports
 -uniques default, non breakable reports only of uniques
 -sat DVBS users only
 -ter DVBT users only
 -app break down by sat
 -app break down by ter
 -apps only

After generating API requests bysed on discovery, all APIs are contacted, all datasets are enhanced with metadata and merged into one big JSON file.

In the end, download option is available. It downloads all data into users computer. Due to securiry configuration, there is no option to stream data automatically into Google Cloud Endpoint. This must hve to bee done manually by uploading file into Google Cloud Storage, where Cloud Function processes all data into Google Cloud Big Query.

## Version 0.1 ##

 - discovery of CRa analytics APIs
 - download all data from CRa
 - basic config available
